package ru.dev_solutions.serg.hrsynchronizer;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import io.reactivex.rxjava3.functions.Consumer;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import polar.com.sdk.api.PolarBleApi;
import polar.com.sdk.api.PolarBleApiCallback;
import polar.com.sdk.api.PolarBleApiDefaultImpl;
import polar.com.sdk.api.errors.PolarInvalidArgument;
import polar.com.sdk.api.model.PolarDeviceInfo;
import polar.com.sdk.api.model.PolarHrBroadcastData;

public class HRActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getSimpleName();

    private String confDeviceId = "";
    private String confHost = "";
    private String confPort = "";
    private String confEndpoint = "";
    private PolarBleApi api;
    private Long lastTimeDataWasSent = 0l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hr);

        this._initConfig();
        this._initApi();
        this._connectToDevice();

        Button btnDisconnect = this.findViewById(R.id.btn_disconnect);
        btnDisconnect.setOnClickListener(v -> {
            this._disconnectFromDevice();
        });

        Button btnCancel = this.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(v -> {
            this._cancel();
        });

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && savedInstanceState == null) {
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    private void _initConfig() {
        Intent intent = getIntent();
        this.confDeviceId = intent.getStringExtra(MainActivity.EXTRA_DEVICE_ID);
        this.confHost = intent.getStringExtra(MainActivity.EXTRA_HOST);
        this.confPort = intent.getStringExtra(MainActivity.EXTRA_PORT);
        this.confEndpoint = intent.getStringExtra(MainActivity.EXTRA_ENDPOINT);
    }

    private void _initApi() {
        api = PolarBleApiDefaultImpl.defaultImplementation(this, PolarBleApi.ALL_FEATURES);
        api.setPolarFilter(false);
        api.setApiLogger(s -> Log.d(TAG, s));

        HRActivity._log( this, "version: " + PolarBleApiDefaultImpl.versionInfo());

        HRActivity context = this;
        String _confHost = this.confHost;
        String _confPort = this.confPort;
        String _confEndpoint = this.confEndpoint;

        api.setApiCallback(new PolarBleApiCallback() {
            @Override
            public void blePowerStateChanged(boolean powered) {
                HRActivity._log(context, "BLE power: " + powered);
            }

            @Override
            public void deviceConnecting(@NonNull PolarDeviceInfo polarDeviceInfo) {
                HRActivity._log(context, "CONNECTING: " + polarDeviceInfo.deviceId);
            }

            @Override
            public void deviceConnected(@NonNull PolarDeviceInfo polarDeviceInfo) {
                Toast.makeText(context, "CONNECTED", Toast.LENGTH_SHORT).show();
                HRActivity._log(context, "CONNECTED: " + polarDeviceInfo.deviceId);
                context.findViewById(R.id.btn_cancel).setVisibility(View.INVISIBLE);
                context.findViewById(R.id.btn_disconnect).setVisibility(View.VISIBLE);

                Set<String> devicesSet = new HashSet<String>();
                devicesSet.add(polarDeviceInfo.deviceId);
                api.startListenForPolarHrBroadcasts(devicesSet).subscribe(new Consumer<PolarHrBroadcastData>() {
                    @Override
                    public void accept(PolarHrBroadcastData polarHrBroadcastData) throws Throwable {
                        Long time = System.currentTimeMillis();
                        if (time - lastTimeDataWasSent >= 1000) {
                            String currentTime = new SimpleDateFormat("HH:mm:ss.mmm", Locale.getDefault()).format(new Date());
                            TextView hrText = context.findViewById(R.id.hr_text);
                            hrText.setText("HR value: " + polarHrBroadcastData.hr + " - " + currentTime);
                            HRActivity._sendHRDataToServer(context, _confHost, _confPort, _confEndpoint, polarHrBroadcastData.hr);
                            lastTimeDataWasSent = time;
                        }
                    }
                });
            }

            @Override
            public void deviceDisconnected(@NonNull PolarDeviceInfo polarDeviceInfo) {
                Toast.makeText(context, "DISCONNECTED", Toast.LENGTH_SHORT).show();
                HRActivity._log(context, "DISCONNECTED: " + polarDeviceInfo.deviceId);
                context._cancel();
            }


            @Override
            public void hrFeatureReady(@NonNull String identifier) {
                HRActivity._log(context, "HR READY: " + identifier);
                // hr notifications are about to start
            }

            @Override
            public void batteryLevelReceived(@NonNull String identifier, int level) {
                HRActivity._log(context, "BATTERY LEVEL: " + level);
            }
        });
    }

    private void _cancel() {
        this.finish();
    }

    private void _connectToDevice() {
        try {
            api.connectToDevice(this.confDeviceId);
        } catch (PolarInvalidArgument polarInvalidArgument) {
            HRActivity._log(this, "CONNECTION FAILED: ");
            polarInvalidArgument.printStackTrace();
        }
    }

    private void _disconnectFromDevice() {
        try {
            api.disconnectFromDevice(this.confDeviceId);
        } catch (PolarInvalidArgument polarInvalidArgument) {
            polarInvalidArgument.printStackTrace();
        }
    }

    private static void _sendHRDataToServer(HRActivity context, String confHost, String confPort, String confEndpoint, Integer hr) {
        String baseUrl = confHost + ":" + confPort + "/" + confEndpoint;

        if (!baseUrl.startsWith("http://") && !baseUrl.startsWith("https://")) {
            baseUrl = "http://" + baseUrl;
        }

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hr", hr.toString())
                .build();
        Request request = new Request.Builder()
                .url(baseUrl)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (!response.isSuccessful()) {
                    HRActivity._logInMainThread(context, "HTTP error, code: " + response.code());
                } else {
                    HRActivity._logInMainThread(context, "HTTP data (hr=" + hr + ") has been sent: " + response.code());
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                HRActivity._logInMainThread(context, "HTTP failure: " + e.getMessage());
            }
        });
    }

    private static void _logInMainThread(HRActivity context, String text) {
        Handler handler = new Handler(context.getMainLooper());
        handler.post( new Runnable() {
            @Override
            public void run() {
                HRActivity._log(context, text);
            }
        } );
    }

    private static void _log(HRActivity context, String text) {
        Log.d(TAG, text);
        TextView loggerTextView = context.findViewById(R.id.logger_text);
        loggerTextView.append(text + "\n");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode == 1) {
            HRActivity._log(this, "BT ready");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        api.backgroundEntered();
    }

    @Override
    public void onResume() {
        super.onResume();
        api.foregroundEntered();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        api.shutDown();
    }
}
