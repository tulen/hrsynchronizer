package ru.dev_solutions.serg.hrsynchronizer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.reactivex.rxjava3.functions.Consumer;
import polar.com.sdk.api.PolarBleApi;
import polar.com.sdk.api.PolarBleApiDefaultImpl;
import polar.com.sdk.api.errors.PolarInvalidArgument;
import polar.com.sdk.api.model.PolarDeviceInfo;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getSimpleName();

    public static final String EXTRA_DEVICE_ID = "ru.dev_solutions.serg.hrsynchronizer.EXTRA_DEVICE_ID";
    public static final String EXTRA_HOST = "ru.dev_solutions.serg.hrsynchronizer.EXTRA_HOST";
    public static final String EXTRA_PORT = "ru.dev_solutions.serg.hrsynchronizer.EXTRA_PORT";
    public static final String EXTRA_ENDPOINT = "ru.dev_solutions.serg.hrsynchronizer.EXTRA_ENDPOINT";

    private static final String SP_DEVICE_ID = "device_id";
    private static final String SP_HOST = "host";
    private static final String SP_PORT = "port";
    private static final String SP_ENDPOINT = "endpoint";

    private SharedPreferences sharedPreferences;
    private PolarBleApi api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this._initConfig();
        this._initApiAndSearchDevice();

        final Button btnConnect = this.findViewById(R.id.connect_button);

        btnConnect.setOnClickListener(v -> {
            this._openHRActivity();
        });

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && savedInstanceState == null) {
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    private void _initConfig() {
        // get the values from SharedPreferences
        this.sharedPreferences = getPreferences(MODE_PRIVATE);

        String deviceId = this.sharedPreferences.getString(SP_DEVICE_ID, "");
        EditText deviceIdInput = this.findViewById(R.id.device_id_input);
        deviceIdInput.setText(deviceId);

        String host = this.sharedPreferences.getString(SP_HOST, "");
        EditText hostInput = this.findViewById(R.id.host_input);
        hostInput.setText(host);

        String port = this.sharedPreferences.getString(SP_PORT, "");
        EditText portInput = this.findViewById(R.id.port_input);
        portInput.setText(port);

        String endpoint = this.sharedPreferences.getString(SP_ENDPOINT, "");
        EditText endpointInput = this.findViewById(R.id.endpoint_input);
        endpointInput.setText(endpoint);
    }

    private void _initApiAndSearchDevice() {
        api = PolarBleApiDefaultImpl.defaultImplementation(this, PolarBleApi.ALL_FEATURES);
        api.setPolarFilter(false);
        api.setApiLogger(s -> Log.d(TAG, s));

        MainActivity context = this;

        api.searchForDevice().subscribe(new Consumer<PolarDeviceInfo>() {
            @Override
            public void accept(PolarDeviceInfo polarDeviceInfo) throws Throwable {
                EditText deviceIdInput = context.findViewById(R.id.device_id_input);
                if (deviceIdInput.getText().toString().equals("")) {
                    deviceIdInput.setText(polarDeviceInfo.deviceId);
                }
            }
        });
    }

    private void _openHRActivity() {
        EditText deviceIdInput = this.findViewById(R.id.device_id_input);
        String deviceId = deviceIdInput.getText().toString();
        EditText hostInput = this.findViewById(R.id.host_input);
        String host = hostInput.getText().toString();
        EditText portInput = this.findViewById(R.id.port_input);
        String port = portInput.getText().toString();
        EditText endpointInput = this.findViewById(R.id.endpoint_input);
        String endpoint = endpointInput.getText().toString();

        if (deviceId.isEmpty()) {
            Toast.makeText(this, R.string.err_device_id_is_missed, Toast.LENGTH_SHORT).show();
            return;
        }

        if (host.isEmpty()) {
            Toast.makeText(this, R.string.err_host_is_missed, Toast.LENGTH_SHORT).show();
            return;
        }

        if (port.isEmpty()) {
            Toast.makeText(this, R.string.err_port_is_missed, Toast.LENGTH_SHORT).show();
            return;
        }

        // save the values in SharedPreferences
        this.sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = this.sharedPreferences.edit();
        ed.putString(SP_DEVICE_ID, deviceId);
        ed.putString(SP_HOST, host);
        ed.putString(SP_PORT, port);
        ed.putString(SP_ENDPOINT, endpoint);
        ed.commit();

        // open activity
        Intent intent = new Intent(this, HRActivity.class);
        intent.putExtra(EXTRA_DEVICE_ID, deviceId);
        intent.putExtra(EXTRA_HOST, host);
        intent.putExtra(EXTRA_PORT, port);
        intent.putExtra(EXTRA_ENDPOINT, endpoint);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode == 1) {
            Toast.makeText(this, "BT ready", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        api.backgroundEntered();
    }

    @Override
    public void onResume() {
        super.onResume();
        api.foregroundEntered();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        api.shutDown();
    }
}
